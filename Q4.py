#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from numpy import linalg
from numpy.linalg import matrix_rank
from numpy.linalg import eigvals
import matplotlib.pyplot as plt


data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
Z_orig = data[:,0:-1]
Z_mean = np.mean(Z_orig, axis=0)
print('Z_mean', Z_mean)
Z_stdev = np.std(Z_orig, axis=0)
Z = (Z_orig - Z_mean) / Z_stdev

print('Z shape',Z.shape)
print('Z',Z)
y=data[:,-1:]
print('y shape', y.shape)
print('y', y)

deno = Z.T @ Z
print('eigvals (X.T @ X) - I', eigvals(deno))
print('rank (X.T @ X) - I', matrix_rank(deno))
Beta_estim = linalg.inv(Z.T @ Z) @ Z.T @ y
Y_estim = Z @ Beta_estim
residu = y - Y_estim
print('Beta_estim: residus2', np.sum(residu**2))
print('Beta_estim: Y_estim', Y_estim[17], y[17])

M = Z
U, s, W = linalg.svd(M)
print('s', s)
S_inv=np.zeros( (11,4898),dtype='float')
S_inv[:11 , :11] = np.diag(1/s)
V = W.T
M_inv = V @ S_inv @ U.T
X_min = M_inv @ y 
print('X_min', X_min)
Y_estim = Z @ X_min
residu = y - Y_estim
print('residus2', np.sum(residu**2))
print('Y_estim', Y_estim[17], y[17])

theta_best_svd, residuals, rank, s = linalg.lstsq(M, y, rcond=None)
print('theta_best_svd\n', theta_best_svd)
Y_estim_lstsq = Z @ theta_best_svd
print('Y_estim_lstsq', Y_estim_lstsq[17], y[17])
print('residuals', residuals)
# residuals = 171983
print('s', s)

#print('Z_mean', Z_mean)
#print('Z_stdev', Z_stdev)
#print('Z_orig', Z_orig[4897])
#print('Z', Z[4897])

def scatterplot():
    plt.figure()
    sz=30
    colormap = plt.cm.gist_ncar #nipy_spectral, Set1,Paired  
    colorst = [colormap(i) for i in np.linspace(0, 0.9, 11)]
    plt.scatter(Z[:,0], y, c=colorst[0], s=sz, marker='|', label='fixed acidity') 
    plt.scatter(Z[:,1], y.T, c=colorst[1], s=sz, marker='|', label='volatile acidity')
    plt.scatter(Z[:,3], y.T, c=colorst[3], s=sz, marker='|', label='residual sugar')
    plt.scatter(Z[:,6], y.T, c=colorst[6], s=sz, marker='|', label='total sulfur dioxide')
    plt.scatter(Z[:,10], y.T, c=colorst[10], s=sz, marker='|', label='alcohol') 
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('White wine quality')
    plt.legend(loc='lower right')
    plt.show()

scatterplot()



