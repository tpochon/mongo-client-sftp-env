#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 07:12:06 2021

@author: etpe7363
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from confmatrxplot import plot_confusion_matrix
from sklearn.metrics import classification_report, confusion_matrix


# https://archive.ics.uci.edu/ml/datasets/wine+quality
data = pd.read_csv("winequality-red.csv", sep=';')
print(data.head())

class PerceptronModel:
    weights = np.array([])
    bias = 0.0
    learning_rate = 1.0

    def __init__(self, initial_weights, initial_bias, learning_rate):
        self.weights = initial_weights
        self.bias = initial_bias
        self.learning_rate = learning_rate
    
    def update_model(self, Xi, yi):
        loss = - yi * self.predict(Xi)
        if (loss >= 0):
            loss_gradient = - yi * Xi
            self.weights = self.weights + (self.learning_rate * (-loss_gradient))
            self.bias = self.bias + self.learning_rate * yi
            #print('KO', loss)
            return 1
        else:
            #print('>>>OK', loss)
            return 0

    def predict(self, Xi):
        return self.weights.T @ Xi + self.bias
    
    def __str__(self):
        return f'w={self.weights}, b={self.bias}'

class perceptron_multiclass_model:
    models = []
    epochs = 1
    classes = []
    learning_rate = 1.0
    
    def __init__(self, learning_rate, nb_epochs, quality_classes):
        self.learning_rate = learning_rate
        self.epochs = nb_epochs
        self.classes = quality_classes

    def train_one_vs_rest(self, Ym):
        w0 = np.zeros((X.shape[1]))
        perceptron_model = PerceptronModel(initial_weights=w0, initial_bias=10, learning_rate=self.learning_rate)
        for epoch in range(self.epochs):
            sum_loss = 0
            for Xi, yi in zip(X, Ym):
                loss = perceptron_model.update_model(Xi, yi)
                sum_loss = sum_loss + loss
            print('accuracy epoch {}'.format(epoch),100-sum_loss*100.0/len(Y))
        print('=> perceptron_model', perceptron_model)
        return perceptron_model

    def train_multiclass(self, X):
        for quality in self.classes:
            Ym = transform_multinomial_one_vs_all(Y, quality)
            print('===== train_one_vs_rest : quality=', quality)
            perceptron_model = self.train_one_vs_rest(Ym)
            self.models.append(perceptron_model)

    def predict(self, Xi):
        scores = []
        for quality, model in zip(self.classes, self.models):
            score = model.predict(Xi)
            scores.append((quality, score))
        best_score = max(scores, key=lambda score: score[1])
        #print(scores, best_score)
        return best_score
    
    def eval(self, X, Y, quality_classes):
        loss = 0
        Y_hat = np.zeros(len(Y), dtype=int)
        for i in range(len(Y)):
            Y_hat[i] = self.predict(X[i])[0]
            loss += 1 if Y_hat[i] != Y[i] else 0
        print('eval : loss multiclass %', loss*100.0/len(Y))
        label_names = ['Quality[{}]'.format(q) for q in quality_classes] 
        print(classification_report(Y, Y_hat, target_names=label_names, zero_division=0))
        

def scatter_feature(data, f1, f2, Ym, quality):
    ycmap = mpl.colors.ListedColormap(['blue','red'])
    scatter = plt.scatter(data[f1], data[f2], c=Ym, cmap=ycmap)
    legend = plt.legend(scatter.legend_elements()[0], scatter.legend_elements()[1])
    legend.get_texts()[0].set_text('other quality')
    legend.get_texts()[1].set_text('quality[{}]'.format(str(quality)))
    plt.xlabel(f1)
    plt.ylabel(f2)
    plt.show()

def transform_multinomial_one_vs_all(Y, quality):
    Ym = Y.copy()
    Ym[Y != quality] = -1
    Ym[Y == quality] = 1
    return Ym

def standardize_data_features(df):
    df_mean = np.mean(df, axis=0)
    df_stdev = np.std(df, axis=0)
    return (df - df_mean) / df_stdev

def sort_features_by_weights(perceptron_model):
    features = data.columns
    I = np.array(range(X.shape[1])).reshape((X.shape[1],1))
    E = np.hstack((I,100*np.absolute(perceptron_model.weights.reshape((X.shape[1],1)))))
    E = np.array(sorted(E, key=lambda a_entry: a_entry[1]))[::-1]
    print('sort features by model weights')
    for feature_name, feature_rank in [(features[int(coef[0])], coef[1]) for coef in E]:
        print("{:{}} : {:.4f}".format(feature_name, 20, feature_rank))

def predict_quality(perceptron_model, Ym, quality):
    loss = 0
    Y_hat = np.zeros(len(Ym), dtype=int)
    for i in range(len(Ym)):
        y_pred = perceptron_model.predict(X[i])
        loss += 1 if y_pred*Ym[i] < 0 else 0
        Y_hat[i] = 1 if y_pred > 0 else -1
        #print('y_pred {:+006.2f} {:+d} => Y_hat = {}, loss = {}'.format(y_pred, Ym[i], Y_hat[i], loss))
    print('loss %', loss*100.0/len(Ym))
    print('quality[{}] %'.format(quality), len(Ym[Ym==1])*100.0/len(Ym))
    return Y_hat

def eval_predict_quality(Ym, Y_hat, quality):
    cnf_matrix = confusion_matrix(Ym, Y_hat, labels=[-1,1])
    label_names = ['other quality', 'Quality[{}]'.format(quality)]
    plot_confusion_matrix(cm=cnf_matrix, normalize=False, 
                          target_names = label_names, title= "Wine Quality Perceptron model Confusion Matrix")
    print(classification_report(Ym, Y_hat, target_names=label_names))

features = [
    'fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar',
    'chlorides', 'free sulfur dioxide', 'total sulfur dioxide', 'density',
    'pH', 'sulphates', 'alcohol'
    ]
print(data.dtypes)
X = standardize_data_features(np.asarray(data[features]))
Y = np.asarray(data['quality'])
print('X', X.shape, 'Y', Y.shape, Y.dtype)

quality_classes = np.unique(Y)
print('Y classes', quality_classes)
learning_rate = 0.01
epochs = 100
multiclass_model = perceptron_multiclass_model(learning_rate, epochs, quality_classes)
multiclass_model.train_multiclass(X)
multiclass_model.eval(X, Y, quality_classes)



# sample test plot on single quality class
quality=6
Ym = transform_multinomial_one_vs_all(Y, quality)
perceptron_model = multiclass_model.train_one_vs_rest(Ym)
Y_hat = predict_quality(perceptron_model, Ym, quality)
#eval_predict_quality(Ym, Y_hat, quality)
i = 37
print('perceptron_model.predict', perceptron_model.predict(X[i]))
print('multiclass_model.predict', multiclass_model.predict(X[i])[0], Y[i])

sort_features_by_weights(perceptron_model)
#scatter_feature(data, 'alcohol', 'sulphates', Ym, quality)


