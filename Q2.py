#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from numpy import linalg
from numpy.linalg import matrix_rank
from numpy.linalg import eigvals


data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
Z = data[:,0:-1]
print('Z shape', Z.shape)
print('Z\n', Z)
y = data[:,-1:]
print('y shape', y.shape)
print('y', y)

deno = Z.T @ Z
print('eigvals (X.T @ X) - I', eigvals(deno))
print('rank (X.T @ X) - I', matrix_rank(deno))
Beta_estim = linalg.inv(Z.T @ Z) @ Z.T @ y
Y_estim = Z @ Beta_estim
print('Beta_estim', Beta_estim)
residu = y - Y_estim
print('Beta_estim: residus2', np.sum(residu**2))
print('Beta_estim: Y_estim', Y_estim[17], y[17])

M = Z
U, s, W = linalg.svd(M)
print('s', s)
S_inv=np.zeros( (11,4898),dtype='float')
S_inv[:11 , :11] = np.diag(1/s)
V = W.T
M_inv = V @ S_inv @ U.T
X_min = M_inv @ y 
print('X_min', X_min)
Y_estim = Z @ X_min
residu = y - Y_estim
print('residus2', np.sum(residu**2))
# residu 2794
print('Y_estim', Y_estim[17], y[17])

theta_best_svd, residuals, rank, s = linalg.lstsq(M, y, rcond=None)
print('theta_best_svd\n', theta_best_svd)
Y_estim_lstsq = Z @ theta_best_svd
print('Y_estim_lstsq', Y_estim_lstsq[17], y[17])
print('residuals', residuals)





