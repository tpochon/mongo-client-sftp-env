#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 21:59:45 2021

@author: etpe7363
"""

import numpy as np
import matplotlib.pyplot as plt
from statistics import NormalDist
from math import log
import sys
import csv

data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
print('data shape', data.shape)

class WineFeaturesGivenQuality:
    def __init__(self, quality, data, prob_quality):
        self.quality = quality
        self.prob_quality = prob_quality
        self.normals = [ NormalDist.from_samples(data[:,x]) for x in range(data.shape[1]) ]
        
    def likelihood(self, sample):
        return log(self.prob_quality) + sum([log(max(self.normals[i].pdf(sample[i]), sys.float_info.min)) for i in range(len(sample))])
        
    def __str__(self):
        return 'WineFeaturesGivenQuality: quality = ' + str(self.quality) + '\nprob_quality = ' + str(self.prob_quality) + '\ngaussian features = \n' + self.normals.__str__() + '\n'

class WineQualityClassifier:
    wine_features = []
    
    def __init__(self, data, log = False):
        y = data[:,-1:]
        for q in np.unique(y):
            dataq = data[ data[:,-1] == q ]
            prob_quality = float(dataq.shape[0])/float(len(y))
            wine_feature = WineFeaturesGivenQuality(q, dataq[:,0:-1], prob_quality)
            if (log):
                print('wine_feature =', wine_feature)
            self.wine_features.append(wine_feature)
        
    def estimateQuality(self, sample):
        wine = max(self.wine_features, key=lambda wine_feature: wine_feature.likelihood(sample))
        return wine.quality

def estimate_sample_quality_accuracy(wqc, test_data_x, test_data_y):
    y_classed = np.array([wqc.estimateQuality(test_data_x[i,:]) for i  in range(test_data_x.shape[0])])
    residu = test_data_y - y_classed
    accuracy = (len(residu)-np.count_nonzero(residu))/len(residu)
    return accuracy

def train_quality_estimation(data, test_percent):
    sample_size = data.shape[0]*test_percent//100
    print('sample_size', sample_size)
    test_data_x, training_data = get_sample_data(data, 0, sample_size)
    wqc = WineQualityClassifier(training_data)
    y = data[0:sample_size,-1:].reshape((sample_size,))
    accuracy_orig = estimate_sample_quality_accuracy(wqc, test_data_x, y)
    return test_data_x, y, wqc, accuracy_orig

def get_sample_data(data, from_test, to_test):
    if (from_test == 0 and to_test == data.shape[0]):
        return (data[:,:-1], data)
    mask = np.ones((data.shape[0],), dtype= bool)
    mask[from_test:to_test] = False
    return (data[from_test:to_test,:-1], data[mask])

def suffle_feature(data_x, feature_i):
    x_permuted = data_x.copy()
    x_permuted[:,feature_i] = np.random.permutation(x_permuted[:,feature_i])
    return x_permuted

def compute_accuracy_stat(accuracies):
    accuracy_mean = np.mean(accuracies, axis=0)
    accuracy_stdev = np.std(accuracies, axis=0)
    return (accuracy_mean, accuracy_stdev)

def histo_feature(accuracies_mean, accuracies_stdev, accuracy_orig):
    with open('winequality-white.csv') as f:
        features = next(csv.reader(f, delimiter=';'))[:-1]
    plt.figure()
    plt.barh(features, accuracies_mean, xerr = accuracies_stdev, ecolor='k', align='center', alpha=0.5, label='permuted')
    plt.xlabel('accuracy')
    plt.ylabel('features')
    plt.title('wine quality feature importance with permutation')
    plt.axvline(x=accuracy_orig, color='r', linestyle='dashed', linewidth=2, label='not permuted')
    plt.legend(loc='lower left')
    plt.show()

def feature_importance_permutation(data_x, y, wqc, accuracy_orig, permut_repet):
    accuracies = np.zeros((permut_repet,data_x.shape[1]), dtype='float')
    for feature_i in range(data_x.shape[1]):
        print('permute feature', feature_i, permut_repet, ' times ', end='')
        for repet_j in range(permut_repet):
            print('.', end='')
            x_permuted = suffle_feature(data_x, feature_i)
            accuracies[repet_j, feature_i] = estimate_sample_quality_accuracy(wqc, x_permuted, y)
        print('')
    return compute_accuracy_stat(accuracies)

# set test_percent to 100 to permute on training data set (not on test data set)
test_data_x, y, wqc, accuracy_orig = train_quality_estimation(data, test_percent = 15)
accuracies_mean, accuracies_stdev = feature_importance_permutation(test_data_x, y, wqc, accuracy_orig, permut_repet = 10)
print('accuracy diff percent', (accuracies_mean-accuracy_orig)*100/accuracy_orig)
histo_feature(accuracies_mean, accuracies_stdev, accuracy_orig)


