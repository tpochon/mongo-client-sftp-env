#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from statistics import NormalDist
from math import log
import sys
from scipy.stats import norm
import csv

data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
print('data shape', data.shape)
samples = data[:,0:-1]

y = data[:,-1:]
print('wine quality multinomial classes', np.unique(y))


class WineFeaturesGivenQuality:
    def __init__(self, quality, data, prob_quality):
        self.quality = quality
        self.prob_quality = prob_quality
        self.normals = [ NormalDist.from_samples(data[:,x]) for x in range(data.shape[1]) ]
        
    def likelihood(self, sample):
        return log(self.prob_quality) + sum([log(max(self.normals[i].pdf(sample[i]), sys.float_info.min)) for i in range(len(sample))])
        
    def __str__(self):
        return 'WineFeaturesGivenQuality: quality = ' + str(self.quality) + '\nprob_quality = ' + str(self.prob_quality) + '\ngaussian features = \n' + self.normals.__str__() + '\n'

class WineQualityClassifier:
    wine_features = []
    
    def __init__(self, data):
        y = data[:,-1:]
        for q in np.unique(y):
            dataq = data[ data[:,-1] == q ]
            prob_quality = float(dataq.shape[0])/float(len(y))
            wine_feature = WineFeaturesGivenQuality(q, dataq[:,0:-1], prob_quality)
            print('wine_feature =', wine_feature)
            self.wine_features.append(wine_feature)
        
    def estimateQuality(self, sample):
        wine = max(self.wine_features, key=lambda wine_feature: wine_feature.likelihood(sample))
        return wine.quality

wqc = WineQualityClassifier(data)
y_classed = np.array([wqc.estimateQuality(samples[i,:]) for i  in range(samples.shape[0])])
y = y.reshape((y.shape[0],))
residu = y - y_classed
print('y        ', y)
print('y_classed', y_classed)
print('residu', residu)
print('accurancy', (len(residu)-np.count_nonzero(residu))/len(residu))
print('residus2', np.sum(residu**2))
# residus2 (naive bayes) > residu2 (least square regression estimate) [-1737]

def histo_feature(data, quality, quality_estim, sample, feature, feature_label, normal):
    dataq = data[ data[:,-1] == quality ]
    dataq_feature = dataq[:,feature]
    val_bins, _, _  = plt.hist(dataq_feature, bins='auto', label='feature samples')
    plt.xlabel(feature_label)
    plt.ylabel('samples distribution / ' + str(dataq_feature.shape[0]))
    plt.title(feature_label + ' | wine quality [' + str(quality) + '] / ^[' + str(quality_estim) + ']')
    plt.axvline(x=sample, color='r', linestyle='dashed', linewidth=2)
    x_norm = np.linspace(min(dataq_feature), max(dataq_feature), 100)
    y_norm = norm(normal.mean, normal.stdev).pdf(x_norm) * normal.stdev * max(val_bins)
    plt.plot(x_norm, y_norm, label='gaussian model')
    plt.legend(loc='upper right')
    plt.show()

def lineplot_quality(Y, Y_estim):
    plt.figure()
    plt.plot(Y, 'b', label='Y')
    plt.plot(Y_estim, 'g', label='Y_estim')
    plt.plot(abs(Y_estim-Y), 'r', label='diff estim')
    plt.xlabel('x samples')
    plt.ylabel('y wine quality')
    plt.title('White wine quality')
    plt.legend(loc='lower right')
    plt.show()

# draw y(quality) and y classed (quality estimated) and diff for each sample
#lineplot_quality(y, y_classed)

# log sample quality estimate
with open('winequality-white.csv') as f:
    features = next(csv.reader(f, delimiter=';'))
i=14
feature=10
feature_label=features[feature]
quality=5
samplei = samples[i,:]
sample_feature = samplei[feature]
qi = y[i]
qi_estim = wqc.estimateQuality(samplei)
normal = wqc.wine_features[int(quality-min(np.unique(y)))].normals[feature]
print('quality =', qi, ', classed quality =', qi_estim, ', sample ' + feature_label + ' =', sample_feature, ', distrib = ', normal)

# draw histogram of a feature and one sample feature value
histo_feature(data, quality, qi_estim, sample_feature, feature, feature_label, normal)


