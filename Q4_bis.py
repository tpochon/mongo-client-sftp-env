#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt
import csv


data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
Z_orig = data[:,0:-1]
Z_mean = np.mean(Z_orig, axis=0)
print('Z_mean', Z_mean)
Z_stdev = np.std(Z_orig, axis=0)
ZT = (Z_orig - Z_mean) / Z_stdev

def addI(z):
    I = np.ones((1,z.shape[0])).T
    E = np.hstack((I,z))
    return E

y = data[:,-1:]

with open('winequality-white.csv') as f:
    features = next(csv.reader(f, delimiter=';'))
    maxlenfeatures = max([len(f) for f in features])

def sort_features_by_theta_best(theta):
    I = np.array(range(theta.shape[0])).reshape((1,theta.shape[0])).T
    E = np.hstack((I,np.absolute(theta)))
    E = np.array(sorted(E, key=lambda a_entry: a_entry[1]))[::-1]
    print('feature rank\n{}'.format('-'*(maxlenfeatures + 9)))
    for feature_name, feature_rank in [(features[int(coef[0])], coef[1]) for coef in E]:
        print("{:{}} : {:.4f}".format(feature_name, maxlenfeatures, feature_rank))

def theta_best(M):
    theta_best_svd, residuals, rank, s = linalg.lstsq(M, y, rcond=None)
    Y_estim_lstsq = M @ theta_best_svd
    print('Y_estim_lstsq', Y_estim_lstsq[18], y[18])
    print('residuals', residuals)
    print('variance', sum((y-np.mean(y))**2))
    print('R2', 1-residuals/sum((y-np.mean(y))**2))
    return theta_best_svd

def lineplot(Y, Y_estim):
    plt.figure()
    plt.plot(Y, 'b', label='Y')
    plt.plot(Y_estim, 'g', label='Y_estim')
    plt.plot(abs(Y_estim-Y), 'r', label='diff estim')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('White wine quality')
    plt.legend(loc='lower right')
    plt.show()
    
def scatter_feature(data, y, feature):
    feat_idx = features.index(feature)
    plt.scatter(data[:,feat_idx], y.reshape(len(y),),  color='blue')
    plt.xlabel(feature)
    plt.ylabel("quality")
    plt.show()    

print('>>>>>> Z')
theta_best_svd_Z_orig = theta_best(Z_orig)
print('>>>>>> ZI')
ZI = addI(Z_orig)
theta_best_svd_ZI = theta_best(ZI)

print('>>>>>> ZT')
theta_best_svd_ZT = theta_best(ZT)
print('>>>>>> ZTI')
ZTI = addI(ZT)
theta_best_svd_ZTI = theta_best(ZTI)

sort_features_by_theta_best(theta_best_svd_ZTI[1:])

#y_estim = Z_orig @ theta_best_svd_Z_orig
#y_estim = ZI @ theta_best_svd_ZI
#y_estim = ZT @ theta_best_svd_ZT
y_estim = ZTI @ theta_best_svd_ZTI
lineplot(y, y_estim)
scatter_feature(ZT, y, 'density')