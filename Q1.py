# -*- coding: utf-8 -*

import numpy as np
from numpy import linalg
from numpy.linalg import matrix_rank
from numpy.linalg import inv
from numpy.linalg import eigvals


y = np.ones((1,8)).T
I = np.identity(8)
E = np.zeros((8,8), dtype='float')
E[0,7] = 1.0
X = I + E
print('X\n', X)


deno = X.T @ X
print('eigvals (X.T @ X) - I', eigvals(deno-I))
print('rank (X.T @ X) - I', matrix_rank(deno-I))
Beta_estim = inv(X.T @ X) @ X.T @ y
print('Beta_estim\n', Beta_estim)
sum_Beta_estim = np.sum(Beta_estim)
print('sum_Beta_estim', sum_Beta_estim)
print('X * Beta_estim\n', X @ Beta_estim)


M = X
U, s, W = linalg.svd(M)
print('s', s)
S_inv = np.diag(1/s)
V = W.T
M_inv = V @ S_inv @ U.T
X_min = M_inv @ y 
print('X_min\n', X_min)
sum_X_min = np.sum(X_min)
print('sum_X_min', sum_X_min)
print('M * X_min\n', M @ X_min)


theta_best_svd, residuals, rank, s = linalg.lstsq(M, y, rcond=1e-6)
print('theta_best_svd\n', theta_best_svd)
print('M * theta_best_svd\n', M @ theta_best_svd)



