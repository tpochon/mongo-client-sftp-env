#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 21:59:45 2021

@author: etpe7363
"""

import numpy as np
from statistics import NormalDist
from math import log
import sys

data = np.genfromtxt('winequality-white.csv', delimiter=';', skip_header=True)
print('data shape', data.shape)

class WineFeaturesGivenQuality:
    def __init__(self, quality, data, prob_quality):
        self.quality = quality
        self.prob_quality = prob_quality
        self.normals = [ NormalDist.from_samples(data[:,x]) for x in range(data.shape[1]) ]
        
    def likelihood(self, sample):
        return log(self.prob_quality) + sum([log(max(self.normals[i].pdf(sample[i]), sys.float_info.min)) for i in range(len(sample))])
        
    def __str__(self):
        return 'WineFeaturesGivenQuality: quality = ' + str(self.quality) + '\nprob_quality = ' + str(self.prob_quality) + '\ngaussian features = \n' + self.normals.__str__() + '\n'

class WineQualityClassifier:
    wine_features = []
    
    def __init__(self, data, log = False):
        y = data[:,-1:]
        for q in np.unique(y):
            dataq = data[ data[:,-1] == q ]
            prob_quality = float(dataq.shape[0])/float(len(y))
            wine_feature = WineFeaturesGivenQuality(q, dataq[:,0:-1], prob_quality)
            if (log):
                print('wine_feature =', wine_feature)
            self.wine_features.append(wine_feature)
        
    def estimateQuality(self, sample):
        wine = max(self.wine_features, key=lambda wine_feature: wine_feature.likelihood(sample))
        return wine.quality

def estimate_sample_quality(data, from_test, to_test):
    test_data_x, training_data = get_sample_data(data, from_test, to_test)
    wqc = WineQualityClassifier(training_data)
    y_classed = np.array([wqc.estimateQuality(test_data_x[i,:]) for i  in range(test_data_x.shape[0])])
    return y_classed

def cross_validation(data_x, test_percent):
    sample_size = data.shape[0]*test_percent//100
    y_classed = np.zeros((data.shape[0],))
    print('sample_size', sample_size)
    for i in range(0,data.shape[0]//sample_size):
        print('test range', (i*sample_size,(i+1)*sample_size))
        y_classed[i*sample_size:(i+1)*sample_size] = estimate_sample_quality(data, i*sample_size, (i+1)*sample_size)
    print('last range', ((i+1)*sample_size, (i+1)*sample_size+data.shape[0]%sample_size))
    y_classed[(i+1)*sample_size:(i+1)*sample_size+data.shape[0]%sample_size] = estimate_sample_quality(data, (i+1)*sample_size, (i+1)*sample_size+data.shape[0]%sample_size)
    return y_classed

def get_sample_data(data, from_test, to_test):
    if (from_test == 0 and to_test == data.shape[0]):
        return (data[:,:-1], data)    
    mask = np.ones((data.shape[0],), dtype= bool)
    mask[from_test:to_test] = False
    return (data[from_test:to_test,:-1], data[mask])

y_classed = cross_validation(data, test_percent = 3)
y = data[:,-1:].reshape((data.shape[0],))
residu = y - y_classed
print('y        ', y)
print('y_classed', y_classed)
print('residu', residu)
print('accurancy cross validation 3%', (len(residu)-np.count_nonzero(residu))/len(residu))
print('RMSE', np.sqrt(np.mean(residu**2)))
print('R2', 1-sum(residu**2)/sum((y-np.mean(y))**2))

y_classed = estimate_sample_quality(data, 0, data.shape[0])
residu = y - y_classed
print('accurancy on training data set', (len(residu)-np.count_nonzero(residu))/len(residu))
print('RMSE on training data set', np.sqrt(np.mean(residu**2)))
print('R2', 1-sum(residu**2)/sum((y-np.mean(y))**2))
